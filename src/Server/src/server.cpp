#pragma once
//#include "server.h"
#include "../../../src/Common/src/GraphicsSystem.h"
#include "../../../src/Common/src/Services.h"
#include "../../../src/Common/src/InputManager.h"
#include "../../../src/Common/src/NetworkManager.h"
#include "../../../src/Common/src/GameObject.h"

#define PLAYER_SIZE_X 10.0f
#define PLAYER_SIZE_Y 60.0f
#define BALL_SIZE 10.0f

#define PLAYER_ONE_START_POS_X 60.0f

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#define PLAYER_TWO_START_POS_X (SCREEN_WIDTH - PLAYER_ONE_START_POS_X)

#define MOVE_SPEED 0.1f

bool g_MoveBall = false;
float g_BallVX = 0.1f;
float g_BallVY = 0.0f;
sf::Vector2f g_BallV = sf::Vector2f(g_BallVX, g_BallVY);
sf::CircleShape g_Ball = sf::CircleShape();

void MoveBall(bool shouldMove);

void main()
{
  sf::Clock theClock;
  sf::Time theTime;
  const float fps = 30.0f;
  float updateTime = 1000 / fps;

  Services* mpServices;
  GraphicsSystem* mpGraphicsSystem = new GraphicsSystem();
  NetworkManager* mpNetworkManager = new NetworkManager();
  InputManager* mpInputSystem = new InputManager();

  mpServices->init();
  mpServices->provideGraphicsSystem(mpGraphicsSystem);
  mpServices->provideNetworkManager(mpNetworkManager);
  mpServices->provideInputSystem(mpInputSystem);

  mpGraphicsSystem->init("BC/DC Games - Server", SCREEN_WIDTH, SCREEN_HEIGHT);
  mpNetworkManager->StartServer(8888, 4);
  mpInputSystem->init();

  ///////////////

  mpNetworkManager->ball.SetTransformBall(Transform(
							sf::Vector3f((mpGraphicsSystem->getWidth() / 2) - (BALL_SIZE/2), (mpGraphicsSystem->getHeight() / 2) - (BALL_SIZE / 2), 0),	// Pos
							  sf::Vector3f(0.0f, 0.0f, 0.0f),	// Rot
							  sf::Vector3f(BALL_SIZE, BALL_SIZE, 0.0f))); // Size

  mpNetworkManager->playerOne = new GameObject;
  mpNetworkManager->playerOne->SetTransform(Transform(
								  sf::Vector3f(PLAYER_ONE_START_POS_X, (SCREEN_HEIGHT / 2) - (PLAYER_SIZE_Y / 2), 0.0f),
								  sf::Vector3f(),
								  sf::Vector3f(PLAYER_SIZE_X, PLAYER_SIZE_Y, 0.0f)));

  mpNetworkManager->playerTwo = new GameObject;
  mpNetworkManager->playerTwo->SetTransform(Transform(
								  sf::Vector3f(PLAYER_TWO_START_POS_X, (SCREEN_HEIGHT / 2) - (PLAYER_SIZE_Y / 2), 0.0f),
								  sf::Vector3f(),
								  sf::Vector3f(PLAYER_SIZE_X, PLAYER_SIZE_Y, 0.0f)));

  
  
  theClock.restart();
  while (mpInputSystem->g_SPlayGame)
  {
	theTime = theClock.getElapsedTime();
	if (theTime.asMilliseconds() > updateTime)
	{
	  if (mpNetworkManager->startBallMove)
	  {
		MoveBall(mpNetworkManager->startBallMove);
	  }

	  mpInputSystem->pollInput();

	  mpNetworkManager->Update();

	  mpGraphicsSystem->clearToColor(sf::Color::Black);
	  mpGraphicsSystem->drawShape(mpNetworkManager->playerOne->GetShape());
	  mpGraphicsSystem->drawShape(mpNetworkManager->playerTwo->GetShape());
	  mpGraphicsSystem->drawShape(mpNetworkManager->ball.GetBall());
	  mpGraphicsSystem->flipDisplay();
	}
  }


  delete mpInputSystem;
  delete mpNetworkManager;
  delete mpGraphicsSystem;

	return;
}

void MoveBall(bool shouldMove)
{
#define PI 3.14159265
  bool reset = false;
  if (shouldMove)
  {
	const float MAX_BOUNCE_ANBLE = 45;
	float xBallHit = Services::getNetworkManager()->ball.GetBall().getPosition().x + (BALL_SIZE / 2);
	float yBallHit = Services::getNetworkManager()->ball.GetBall().getPosition().y + (BALL_SIZE / 2);
	sf::Vector2f ballCenter = sf::Vector2f(xBallHit, yBallHit);

	if (ballCenter.x >= 60 && ballCenter.x <= SCREEN_WIDTH - 50)
	{
	  if (ballCenter.x < Services::getNetworkManager()->playerOne->GetTransform().GetPosition().x + 25 && (ballCenter.y >= Services::getNetworkManager()->playerOne->GetTransform().GetPosition().y && ballCenter.y <= Services::getNetworkManager()->playerOne->GetTransform().GetPosition().y + 60.0f))
	  {

		float relativeIntersectY = (Services::getNetworkManager()->playerOne->GetTransform().GetPosition().y + (60.0f / 2)) - ballCenter.y;
		//std::cout << std::endl << relativeIntersectY;

		if (relativeIntersectY >= -30 && relativeIntersectY <= 30)
		{
		  float normalizedRelativeIntersectionY = (relativeIntersectY / (60.0f / 2));
		  float bounceAngle = normalizedRelativeIntersectionY * 75.0;

		  g_BallV.x = 0.1f*cos(bounceAngle);
		  g_BallV.y = 0.1f*sin(bounceAngle);
		}

	  }

	  else if (ballCenter.x > Services::getNetworkManager()->playerTwo->GetTransform().GetPosition().x && (ballCenter.y >= Services::getNetworkManager()->playerTwo->GetTransform().GetPosition().y && ballCenter.y <= Services::getNetworkManager()->playerTwo->GetTransform().GetPosition().y + 60.0f))
	  {

		float relativeIntersectY = (Services::getNetworkManager()->playerTwo->GetTransform().GetPosition().y + (60.0f / 2)) - ballCenter.y;
		//std::cout << std::endl << relativeIntersectY;

		if (relativeIntersectY >= -30 && relativeIntersectY <= 30)
		{
		  float normalizedRelativeIntersectionY = (relativeIntersectY / (60.0f / 2));
		  float bounceAngle = normalizedRelativeIntersectionY * 75.0;

		  g_BallV.x = 0.1f*cos(bounceAngle);
		  g_BallV.y = 0.1f*sin(bounceAngle);
		  g_BallV.x *= -1;
		}

	  }

	  else if (ballCenter.y <= 0)
	  {
		g_BallV.y *= -1;
	  }

	  else if (ballCenter.y >= SCREEN_HEIGHT)
	  {
		g_BallV.y *= -1;
	  }

	  else {}
	}

	if (ballCenter.x <= -10)
	{
	  g_BallV = sf::Vector2f(0.1f, 0);
	  reset = true;
	}

	else if (ballCenter.x > SCREEN_WIDTH + 10)
	{
	  g_BallV = sf::Vector2f(-0.1f, 0);
	  reset = true;
	}

	else {}

	//if(g_Ball.getPosition.x - (BALL_SIZE / 2))
	//float relativeIntersectY = (g_PlayerPaddle.getPosition().y + (20.0f / 2)) - 10;

	if (!reset)
	  Services::getNetworkManager()->ball.GetBall().setPosition(sf::Vector2f(Services::getNetworkManager()->ball.GetBall().getPosition().x + g_BallV.x, Services::getNetworkManager()->ball.GetBall().getPosition().y + g_BallV.y));
	  // update transform of the ball
	else
	  Services::getNetworkManager()->ball.GetBall().setPosition((SCREEN_WIDTH / 2) - BALL_SIZE, (SCREEN_HEIGHT / 2) - BALL_SIZE);

	Services::getNetworkManager()->ball.SetTransformBall(Transform(
														  sf::Vector3f(Services::getNetworkManager()->ball.GetBall().getPosition().x, Services::getNetworkManager()->ball.GetBall().getPosition().y, 0.0f),
														  sf::Vector3f(),
														  sf::Vector3f(BALL_SIZE, BALL_SIZE, 0.0f)));
  }
}