#pragma once
#include "client.h"
#include "../../../src/Common/src/GraphicsSystem.h"
#include "../../../src/Common/src/Services.h"
#include "../../../src/Common/src/NetworkManager.h"
#include <SFML/Graphics/Font.hpp>
#include <SFML/System/Vector3.hpp>


#define PLAYER_SIZE_X 10.0f
#define PLAYER_SIZE_Y 60.0f
#define BALL_SIZE 10.0f

#define SCREEN_HEIGHT 600
#define SCREEN_WIDTH 800

#define MOVE_SPEED 0.1f

bool g_MoveBall = false;
float g_BallVX = 0.1f;
float g_BallVY = 0.0f;
sf::Vector2f g_BallV = sf::Vector2f(g_BallVX, g_BallVY);
sf::RectangleShape g_EnemyPaddle;
sf::RectangleShape g_PlayerPaddle;
sf::CircleShape g_Ball = sf::CircleShape();
int g_PlayerScore = 0, g_EnemyScore = 0;
bool g_IncScore = true;


void DebugSinglePlayer(sf::RectangleShape& player, sf::RectangleShape& enemy);
void MoveBall(bool shouldMove);

void main(bool singlePlayer = false)
{

	sf::Clock theClock;
	sf::Time theTime, startTime;
	const float fps = 30.0f;
	float updateTime = 1000 / fps;

	GraphicsSystem* mpGraphicsSystem = new GraphicsSystem();
	InputManager* mpInputSystem = new InputManager();
	NetworkManager* mpNetworkManager = new NetworkManager();
	Services* mpServices;

	sf::Color textColor = sf::Color::White;
	std::string playerScore;
	std::string enemyScore;

	mpServices->init();
	mpGraphicsSystem->init("BC/DC Games - Client", SCREEN_WIDTH, SCREEN_HEIGHT);
	mpInputSystem->init();
	mpServices->provideGraphicsSystem(mpGraphicsSystem);
	mpServices->provideInputSystem(mpInputSystem);
	mpServices->provideNetworkManager(mpNetworkManager);

	mpNetworkManager->StartClient("localhost", 8888);

	float ballSpeed = 0.1f;

	sf::Vector2f paddleDimensions = sf::Vector2f(PLAYER_SIZE_X, PLAYER_SIZE_Y);
	sf::Vector2f playerStartPosition = sf::Vector2f(60.0f, (SCREEN_HEIGHT / 2) - (PLAYER_SIZE_Y / 2));
	sf::Vector2f enemyStartPosition = sf::Vector2f(SCREEN_WIDTH - 60.0f, (SCREEN_HEIGHT / 2) - (PLAYER_SIZE_Y / 2));
	sf::Vector2f ballStartPosition = sf::Vector2f((SCREEN_WIDTH / 2) - BALL_SIZE, (SCREEN_HEIGHT / 2) - BALL_SIZE);

	//sf::Vector2f playerPos;
	//sf::Vector2f enemyPos;
	//sf::Vector2f ballPos;

	g_PlayerPaddle = sf::RectangleShape(paddleDimensions);
	g_PlayerPaddle.setOutlineColor(sf::Color::Red);
	g_PlayerPaddle.setOutlineThickness(2);
	g_PlayerPaddle.setPosition(playerStartPosition);
	GameObject PPaddle(Transform(sf::Vector3f(playerStartPosition.x, playerStartPosition.y, 0.0f),
								 sf::Vector3f(),
								 sf::Vector3f(paddleDimensions.x, paddleDimensions.y, 0.0f)));
	mpNetworkManager->cPlayer = PPaddle;
	mpNetworkManager->cPlayer.GetShape().setOutlineColor(sf::Color::Red);
	mpNetworkManager->cPlayer.GetShape().setOutlineThickness(2);
	
	g_EnemyPaddle = sf::RectangleShape(paddleDimensions);
	g_EnemyPaddle.setOutlineColor(sf::Color::Blue);
	g_EnemyPaddle.setOutlineThickness(2);
	g_EnemyPaddle.setPosition(enemyStartPosition);
	GameObject EPaddle(Transform(sf::Vector3f(enemyStartPosition.x, enemyStartPosition.y, 0.0f),
								 sf::Vector3f(),
								 sf::Vector3f(paddleDimensions.x, paddleDimensions.y, 0.0f)));
	mpNetworkManager->cEnemy = EPaddle;
	mpNetworkManager->cEnemy.GetShape().setOutlineColor(sf::Color::Blue);
	mpNetworkManager->cEnemy.GetShape().setOutlineThickness(2);

	
	g_Ball.setRadius(BALL_SIZE);
	g_Ball.setOutlineColor(sf::Color::White);
	g_Ball.setFillColor(sf::Color::White);
	g_Ball.setOutlineThickness(1);
	g_Ball.setPosition(ballStartPosition);
	
	bool switched = false;

	mpNetworkManager->newObj = GameObject(Transform(
	  sf::Vector3f(10.0f, 15.f, 0.0f),
	  sf::Vector3f(2.0f, 3.0f, 4.0f),
	  sf::Vector3f(20.0f, 25.0f, 0.0f)));

	singlePlayer = false;
	startTime = theClock.restart();
	mpNetworkManager->cBall.SetTransformBall(Transform(
											sf::Vector3f(SCREEN_WIDTH/2, 1000.0f, 0.0f),
											sf::Vector3f(),
											sf::Vector3f(BALL_SIZE, BALL_SIZE, 0.0f)));
	while (mpInputSystem->g_CPlayGame) // While game isn't over
	{
		theTime = theClock.getElapsedTime();

		if (theTime.asMilliseconds() > updateTime)
		{
			//Switching to single player for debug
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1))
			{
				mpInputSystem->g_CPlayGame = false;
				singlePlayer = true;
			}
			//


			// Client Side Extrapolation
			if (Services::getNetworkManager()->hasGameStarted)
			{
				MoveBall(true);
			}
			// Otherwise it will use the networked movement instead.

			Services::getInputSystem()->pollInput(true, theTime.asSeconds(), mpNetworkManager->cPlayer.GetShape());


			//////////////////////////////////////////////////////////////////////////
			//if (theTime.asMilliseconds() > updateTime / 3)
			//{
				//m_move = Move(mpInputSystem->inputState, theTime.asMilliseconds(), updateTime / 3);
			//}
			//////////////////////////////////////////////////////////////////////////

			mpNetworkManager->Update();

			if (mpNetworkManager->m_PlayerNumber == 2 && !switched)
			{
			  switched = true;

			  mpNetworkManager->cPlayer.SetTransform(Transform(
				sf::Vector3f(enemyStartPosition.x, enemyStartPosition.y, 0.0f),
				sf::Vector3f(),
				sf::Vector3f(paddleDimensions.x, paddleDimensions.y, 0.0f)));

			  mpNetworkManager->cEnemy.SetTransform(Transform(
				sf::Vector3f(playerStartPosition.x, playerStartPosition.y, 0.0f),
				sf::Vector3f(),
				sf::Vector3f(paddleDimensions.x, paddleDimensions.y, 0.0f)));
			}

			if (mpNetworkManager->cBall.GetTransform().GetPosition().x <= 0)
			{
				if (g_IncScore)
				{
					
					g_EnemyScore++;
					g_IncScore = false;
				}
			}

			if (mpNetworkManager->cBall.GetTransform().GetPosition().x >= SCREEN_WIDTH)
			{
				if (g_IncScore)
				{
					g_PlayerScore++;
					g_IncScore = false;
				}
			}

			if (!g_IncScore)
			{
				if (mpNetworkManager->cBall.GetTransform().GetPosition().x >= 60 && mpNetworkManager->cBall.GetTransform().GetPosition().x <= SCREEN_WIDTH)
					g_IncScore = true;
			}

			playerScore = std::to_string(g_PlayerScore);
			enemyScore = std::to_string(g_EnemyScore);
		}

		mpGraphicsSystem->getWindow()->clear(sf::Color::Black);
		mpGraphicsSystem->writeText(playerScore, 60, textColor, sf::Vector2f(300, 100));
		mpGraphicsSystem->writeText(enemyScore, 60, textColor, sf::Vector2f(465, 100));
		mpGraphicsSystem->drawShape(mpNetworkManager->cPlayer.GetShape());
		mpGraphicsSystem->drawShape(mpNetworkManager->cEnemy.GetShape());
		mpGraphicsSystem->drawShape(mpNetworkManager->cBall.GetBall());
		mpGraphicsSystem->getWindow()->display();
	}

	if (singlePlayer)
	{

		mpInputSystem->g_CPlayGame = true;
		std::cout << std::endl << "ENABLED SINGLE PLAYER";
		theClock.restart();

		while (mpInputSystem->g_CPlayGame) // While game isn't over
		{
			theTime = theClock.getElapsedTime();

			if (theTime.asMilliseconds() > updateTime)
			{

				DebugSinglePlayer(mpNetworkManager->cPlayer.GetShape(), mpNetworkManager->cEnemy.GetShape());

				MoveBall(g_MoveBall);

				if (g_Ball.getPosition().x <= 0)
				{
					if (g_IncScore)
					{
						g_PlayerScore++;
						g_IncScore = false;
					}
				}

				if (g_Ball.getPosition().x >= SCREEN_WIDTH)
				{
					if (g_IncScore)
					{
						g_EnemyScore++;
						g_IncScore = false;
					}
				}

				if (!g_IncScore)
				{
					if (g_Ball.getPosition().x >= 60 && g_Ball.getPosition().x <= SCREEN_WIDTH)
						g_IncScore = true;
				}

				playerScore = std::to_string(g_PlayerScore);
				enemyScore = std::to_string(g_EnemyScore);
			}


			mpGraphicsSystem->getWindow()->clear(sf::Color::Black);
			mpGraphicsSystem->writeText(playerScore ,60, textColor, sf::Vector2f(300,100));
			mpGraphicsSystem->writeText(enemyScore, 60, textColor, sf::Vector2f(465, 100));
			mpGraphicsSystem->drawShape(mpNetworkManager->cPlayer.GetShape());
			mpGraphicsSystem->drawShape(mpNetworkManager->cEnemy.GetShape());
			mpGraphicsSystem->drawShape(g_Ball);
			mpGraphicsSystem->getWindow()->display();
		}
	}
	
	delete mpNetworkManager;
	delete mpInputSystem;
	delete mpGraphicsSystem;

}


void DebugSinglePlayer(sf::RectangleShape& player, sf::RectangleShape& enemy)
{

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
  {
	player.setPosition(sf::Vector2f(player.getPosition().x, player.getPosition().y - MOVE_SPEED));
	if (player.getPosition().y < 0)
	  player.setPosition(sf::Vector2f(player.getPosition().x, 0));

	Services::getNetworkManager()->cPlayer.SetTransform(Transform(
	  sf::Vector3f(player.getPosition().x, player.getPosition().y, 0.0f),
	  sf::Vector3f(),
	  sf::Vector3f(PLAYER_SIZE_X, PLAYER_SIZE_Y, 0.0f)));

	//std::cout << "Trans pos: " << Services::getNetworkManager()->cPlayer.GetTransform().GetPosition().y << "\n";
	//std::cout << "obj pos: " << player.getPosition().y << "\n";
	// Need to update transforms position as well because only the shape of the gameobjects position is being changed.
  }
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
  {
	player.setPosition(sf::Vector2f(player.getPosition().x, player.getPosition().y + MOVE_SPEED));
	if (player.getPosition().y > SCREEN_HEIGHT - PLAYER_SIZE_Y)
	  player.setPosition(sf::Vector2f(player.getPosition().x, SCREEN_HEIGHT - PLAYER_SIZE_Y));
	//std::cout << Services::getNetworkManager()->cPlayer.GetTransform().GetPosition().y << "\n";
	//std::cout << "obj pos: " << player.getPosition().y << "\n";

	Services::getNetworkManager()->cPlayer.SetTransform(Transform(
	  sf::Vector3f(player.getPosition().x, player.getPosition().y, 0.0f),
	  sf::Vector3f(),
	  sf::Vector3f(PLAYER_SIZE_X, PLAYER_SIZE_Y, 0.0f)));
  }

  sf::Event inputEvent;

  while (Services::getGraphicsSystem()->getWindow()->pollEvent(inputEvent))
  {
	switch (inputEvent.type)
	{
	case sf::Event::KeyPressed:
	{
	  if (inputEvent.key.code == sf::Keyboard::Escape)
	  {
		  Services::getInputSystem()->g_CPlayGame = false;
	  }
	  else if (inputEvent.key.code == sf::Keyboard::Space)
	  {
		g_MoveBall = true;
	  }
	  break;
	}

	case sf::Event::Closed:
	{
	  Services::getGraphicsSystem()->getWindow()->close();
	  break;
	}
	}
  }
}

void MoveBall(bool shouldMove)
{
#define PI 3.14159265
	bool reset = false;
	if (shouldMove)
	{
		const float MAX_BOUNCE_ANBLE = 45;
		float xBallHit = g_Ball.getPosition().x + (BALL_SIZE / 2);
		float yBallHit = g_Ball.getPosition().y + (BALL_SIZE / 2);
		sf::Vector2f ballCenter = sf::Vector2f(xBallHit, yBallHit);
		
		if (ballCenter.x >= 60 && ballCenter.x <= SCREEN_WIDTH -50)
		{
			if (ballCenter.x < Services::getNetworkManager()->cPlayer.GetTransform().GetPosition().x + 25 && (ballCenter.y >= Services::getNetworkManager()->cPlayer.GetTransform().GetPosition().y && ballCenter.y <= Services::getNetworkManager()->cPlayer.GetTransform().GetPosition().y + 60.0f))
			{

				float relativeIntersectY = (Services::getNetworkManager()->cPlayer.GetTransform().GetPosition().y + (60.0f / 2)) - ballCenter.y;
				//std::cout << std::endl << relativeIntersectY;

				if (relativeIntersectY >= -30 && relativeIntersectY <= 30)
				{
					float normalizedRelativeIntersectionY = (relativeIntersectY / (60.0f / 2));
					float bounceAngle = normalizedRelativeIntersectionY * 75.0;

					g_BallV.x = 0.1f*cos(bounceAngle);
					g_BallV.y = 0.1f*sin(bounceAngle);
				}

			}

			else if (ballCenter.x > Services::getNetworkManager()->cEnemy.GetTransform().GetPosition().x && (ballCenter.y >= Services::getNetworkManager()->cEnemy.GetTransform().GetPosition().y && ballCenter.y <= Services::getNetworkManager()->cEnemy.GetTransform().GetPosition().y + 60.0f))
			{

				float relativeIntersectY = (Services::getNetworkManager()->cEnemy.GetTransform().GetPosition().y + (60.0f / 2)) - ballCenter.y;
				//std::cout << std::endl << relativeIntersectY;

				if (relativeIntersectY >= -30 && relativeIntersectY <= 30)
				{
					float normalizedRelativeIntersectionY = (relativeIntersectY / (60.0f / 2));
					float bounceAngle = normalizedRelativeIntersectionY * 75.0;

					g_BallV.x = 0.1f*cos(bounceAngle);
					g_BallV.y = 0.1f*sin(bounceAngle);
					g_BallV.x *= -1;
				}

			}

			else if (ballCenter.y <= 0)
			{
				g_BallV.y *= -1;
			}

			else if (ballCenter.y >= SCREEN_HEIGHT)
			{
				g_BallV.y *= -1;
			}

			else {}
		}
		
		if (ballCenter.x <= -10)
		{
			g_BallV = sf::Vector2f(0.1f, 0);
			reset = true;
		}

		else if (ballCenter.x > SCREEN_WIDTH + 10)
		{
			g_BallV = sf::Vector2f(-0.1f, 0);
			reset = true;
		}

		else {}

		//if(g_Ball.getPosition.x - (BALL_SIZE / 2))
		//float relativeIntersectY = (g_PlayerPaddle.getPosition().y + (20.0f / 2)) - 10;
	
		if (!reset)
			g_Ball.setPosition(sf::Vector2f(g_Ball.getPosition().x + g_BallV.x, g_Ball.getPosition().y + g_BallV.y));
		else
			g_Ball.setPosition((SCREEN_WIDTH / 2) - BALL_SIZE, (SCREEN_HEIGHT / 2) - BALL_SIZE);
	}
}