#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "../../../src/Common/src/Transform.h"
#include "SFML/Graphics.hpp"
#include <iostream>
#include "RakNet/BitStream.h"

class GameObject
{
public:
  GameObject();
  GameObject(Transform aTransform);
  GameObject(sf::RectangleShape aShape);
  GameObject(Transform aTransform, sf::RectangleShape aShape);
  GameObject(sf::CircleShape);
  ~GameObject();

  bool Intersects(GameObject aGameObject);

  sf::RectangleShape& GetShape() { return _mRec; }
  sf::CircleShape& GetBall() { return _mBall; }
  Transform GetTransform() { return _mTransform; }

  void SetTransform(Transform aTransform) { 
											_mTransform = aTransform;
											_mRec.setSize(sf::Vector2f(_mTransform.GetScale().x, _mTransform.GetScale().y));
											_mRec.setPosition(_mTransform.GetPosition().x, _mTransform.GetPosition().y);
										  }
  void SetTransformBall(Transform aTransform) {
												_mTransform = aTransform;
												_mBall.setRadius(_mTransform.GetScale().x);
												_mBall.setPosition(_mTransform.GetPosition().x, _mTransform.GetPosition().y);
											  }
  Transform GetTransformBall() { return _mTransform; }

  void Serialize(bool writeToBitstream, RakNet::BitStream* bs);

  inline uint32_t ConvertToFixed(float inNumber, float inMin, float inPrecision)
  {
	return static_cast<uint32_t>((inNumber - inMin) / inPrecision);
  }

  inline float ConvertFromFixed(uint32_t inNumber, float inMin, float inPrecision)
  {
	return static_cast<float>((inNumber) * inPrecision + inMin);
  }

private:
  Transform _mTransform;
  sf::RectangleShape _mRec;
  sf::Vector3f _mVelocity;
  sf::CircleShape _mBall;
  //sf::Sprite _mSprite;
  //sf::Texture _mTexture;
};

#endif GAME_OBJECT_H