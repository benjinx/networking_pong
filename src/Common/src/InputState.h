#ifndef INPUT_STATE_H
#define INPUT_STATE_H

#include "RakNet/BitStream.h"

class InputState {
public: 
	InputState() :
		//mDesiredRightAmount(0),
		//mDesiredLeftAmount(0),
		mDesiredForwardAmount(0),
		mDesiredBackAmount(0)//,
		//mIsShooting(false)
		{}

		//float GetDesiredHorizontalDelta() const { return mDesiredRightAmount - mDesiredLeftAmount; }
		float GetDesiredVerticalDelta() const { return mDesiredForwardAmount - mDesiredBackAmount; }
		//bool IsShooting() const { return mIsShooting; }
		void Write(RakNet::BitStream& inOutputStream) const
		{
			inOutputStream.Serialize(true, mDesiredForwardAmount);
			inOutputStream.Serialize(true, mDesiredBackAmount);
		}
		void Read(RakNet::BitStream& inInputStream)
		{
			inInputStream.Serialize(false, mDesiredForwardAmount);
			inInputStream.Serialize(false, mDesiredBackAmount);
		}

private:
	friend class InputManager;
	//float mDesiredRightAmount, mDesiredLeftAmount;
	float mDesiredForwardAmount, mDesiredBackAmount;
	//bool mIsShooting;
};

#endif INPUT_STATE_H