#include "NetworkManager.h"

NetworkManager::NetworkManager()
	: mp_Peer(RakNet::RakPeerInterface::GetInstance())
	, m_CurrentPlayerCount(0)
{
}

void NetworkManager::StartServer(int aServerPort, int aMaxClients)
{
	m_IsServer = true;
	m_SocketDescriptor = RakNet::SocketDescriptor(aServerPort, 0);

	printf("Starting the Server...\n");
	mp_Peer->Startup(aMaxClients, &m_SocketDescriptor, 1);

	mp_Peer->SetMaximumIncomingConnections(aMaxClients);
}

void NetworkManager::StartClient(std::string anIPAddress, int aServerPort)
{
	m_IsServer = false;
	m_SocketDescriptor = RakNet::SocketDescriptor();

	printf("Starting the Client...\n");
	mp_Peer->Startup(1, &m_SocketDescriptor, 1);

	mp_Peer->Connect(anIPAddress.c_str(), aServerPort, 0, 0);
}

void NetworkManager::ProcessPacket(RakNet::BitStream& inInputStream, const RakNet::SystemAddress& inFromAddress)
{
	auto itr = m_AddressToClientMap.find(inFromAddress);
	if (itr == m_AddressToClientMap.end())
	{
		HandlePacketFromNewClient(inInputStream, inFromAddress);
	}
// 	else
// 	{
// 		ProcessPacket((*itr).second, inInputStream);
// 	}
}

void NetworkManager::HandlePacketFromNewClient(RakNet::BitStream& inInputStream, const RakNet::SystemAddress& inFromAddress)
{
	uint32_t packetType;
	inInputStream.Read(packetType);
	std::cout << "Checking packet Type\n";
	if (packetType == PacketType::PT_HELLO)
	{
		std::cout << "Packet Type checked\n";
		std::string name;
		inInputStream.Read(name);
		
		// Create a client proxy
		m_AddressToClientMap.insert(std::make_pair(inFromAddress, new GameObject()));

		auto itr = m_AddressToClientMap.find(inFromAddress);

		if (itr != m_AddressToClientMap.end()) // check to make sure it doesn't crash
		{
			// and welcome the client...
			SendWelcomePacket((*itr).second);

			// init replication manager for this client
			// ...
		}
	}
	else
	{
		std::cout << "Bad incoming packet from unknown client at socket " << inFromAddress.ToString() << "\n";
	}
}

void NetworkManager::SendWelcomePacket(GameObject* newClientProxy)
{

	if (m_CurrentPlayerCount == 0)
	{
		m_PlayerAddresses.insert(std::make_pair("Player1", mp_Packet->systemAddress));
	}
	else
	{
		m_PlayerAddresses.insert(std::make_pair("Player2", mp_Packet->systemAddress));
	}

	m_CurrentPlayerCount += 1;

	//Accepted connection now telling player which number they are
	RakNet::RakString rs;
	RakNet::BitStream bsOut;
	rs = (std::to_string(m_CurrentPlayerCount)).c_str();
	bsOut.Write((RakNet::MessageID)ID_PLAYER_NUM);
	bsOut.Write(rs);
	mp_Peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mp_Packet->systemAddress, false);
}

void NetworkManager::SendInputPacket(RakNet::BitStream& outOutputStream)
{
	RakNet::RakString rs;

	for (int i = 0; i < 3; i++)
	{
		rs = std::to_string(moveListTimeStamps[i]).c_str();
		outOutputStream.Write(rs);
		//std::cout << "in: " << moveListTimeStamps[i] << std::endl;
	}
}

void NetworkManager::RecieveInputPacket(RakNet::BitStream& inInputStream)
{
	RakNet::RakString rs;
	for (int i = 0; i < 3; i++)
	{
		inInputStream.Read(rs);
		moveListTimeStamps[i] = std::stoi(rs.C_String());
		//std::cout << "Out: " << moveListTimeStamps[i] << std::endl;
	}
}

void NetworkManager::VerifyMove(GameObject* player)
{
	if (moveList.size() > 0 && moveList.size() < 3)
	{
		if (player == playerOne)
			player->SetTransform(Transform(playerStartPosition.x, moveList.at(0).GetInputState().GetDesiredVerticalDelta(), 0.0f, 0.0f, 0.0f, 0.0f, paddleDimensions.x, paddleDimensions.y, 0.0f));
		else if (player == playerTwo)
			player->SetTransform(Transform(enemyStartPosition.x, moveList.at(0).GetInputState().GetDesiredVerticalDelta(), 0.0f, 0.0f, 0.0f, 0.0f, paddleDimensions.x, paddleDimensions.y, 0.0f));
		std::cout << "Player Trans y: " << player->GetTransform().GetPosition().y;
	}
}

void NetworkManager::AddMove(Move& inMove)
{
	moveList.push_back(inMove);
	
	for (int i = 0; i < 3; i++)
	{
		moveListTimeStamps[i] = inMove.GetTimestamp();
		//std::cout << "TimeStamp: " << moveListTimeStamps[i] << std::endl;
	}
}

void NetworkManager::Update()
{
	for (mp_Packet = mp_Peer->Receive(); mp_Packet; mp_Peer->DeallocatePacket(mp_Packet), mp_Packet = mp_Peer->Receive())
	{
		switch (mp_Packet->data[0])
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
		{
			std::cout << "Another client has disconnected.\n";
			break;
		}
		case ID_REMOTE_CONNECTION_LOST:
		{
			std::cout << "Another client has lost the connection.\n";
			break;
		}
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
		{
			std::cout << "Another client has connected.\n";
			break;
		}
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			std::cout << "Our connection request has been accepted.\n";
			break;
		}
		case ID_NEW_INCOMING_CONNECTION:
		{
			std::cout << "A new connection is incoming.\n";
			RakNet::BitStream bsIn;
			bsIn.Write((PacketType)PT_HELLO);
			ProcessPacket(bsIn, mp_Packet->systemAddress);
			break;
		}
		case ID_NO_FREE_INCOMING_CONNECTIONS:
		{
			std::cout << "The server is full.\n";
			break;
		}
		case ID_DISCONNECTION_NOTIFICATION:
		{
			std::cout << "A client has disconnected.\n";
			break;
		}
		case ID_CONNECTION_LOST:
		{
			std::cout << "A client lost the connection.\n";
			break;
		}
		case ID_GAME_START_S:
		{
			RakNet::BitStream bsIn(mp_Packet->data, mp_Packet->length, false);
		    bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			std::cout << "Start server!\n";

			// Set bool of move ball to true
			startBallMove = true;
			hasGameStarted = true;

			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_GAME_START_C);
			ball.Serialize(true, &bsOut);
			bsOut.Write(hasGameStarted);
			mp_Peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
			break;
		}
		case ID_GAME_START_C:
		{
		  RakNet::BitStream bsIn(mp_Packet->data, mp_Packet->length, false);
		  bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
		  cBall.Serialize(false, &bsIn);
		  bsIn.Read(hasGameStarted);
							  
		  std::cout << "Start the game for clients!\n";

		  // Send player pos to the server here.
		  RakNet::RakString rs;
		  RakNet::BitStream bsOut;
		  bsOut.Write((RakNet::MessageID)ID_UPDATE_S);
		  rs = std::to_string(m_PlayerNumber).c_str();
		  bsOut.Write(rs);
		  SendInputPacket(bsOut);
		  cPlayer.Serialize(true, &bsOut);
		  mp_Peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mp_Packet->systemAddress, false);
		  break;
		}
		case ID_UPDATE_S:
		{
		  bool canMove = false;
		  RakNet::RakString rs;
		  RakNet::BitStream bsIn(mp_Packet->data, mp_Packet->length, false);
		  bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
		  
		  bsIn.Read(rs);
		  int playerNum = std::stoi(rs.C_String());

		  RecieveInputPacket(bsIn);

		  // Combine the word Player with the number associated with the player so we can access the m_PlayerAddress
		  std::string player = "Player" + std::to_string(playerNum);

		  if (playerNum == 1)
		  {
			auto playerOneAddress = m_PlayerAddresses.find(player)->second;
			playerOne = m_AddressToClientMap.find(playerOneAddress)->second;

			// Save old position
			sf::Vector3f startMarker = playerOne->GetTransform().GetPosition();
			// Get new pos
			playerOne->Serialize(false, &bsIn);
			Transform oldTrans = playerOne->GetTransform();

			// Save new pos
			sf::Vector3f endMarker = playerOne->GetTransform().GetPosition();

			// Lerp
			float x = lerp(startMarker.x, endMarker.x, m_cTimeStamp);
			float y = lerp(startMarker.y, endMarker.y, m_cTimeStamp);

			// Update Position
			playerOne->SetTransform(Transform(sf::Vector3f(x, y, 0), playerOne->GetTransform().GetRotation(), playerOne->GetTransform().GetScale()));
			playerOne->SetTransform(oldTrans);

			//VerifyMove(playerOne);
			canMove = VerifyMove();
		  }
		  else if (playerNum == 2)
		  {
			  auto playerTwoAddress = m_PlayerAddresses.find(player)->second;
			  playerTwo = m_AddressToClientMap.find(playerTwoAddress)->second;

			  // Save old position
			  sf::Vector3f startMarker = playerTwo->GetTransform().GetPosition();
			  // Get new pos
			  playerTwo->Serialize(false, &bsIn);
			  Transform oldTrans = playerTwo->GetTransform();

			  // Save new pos
			  sf::Vector3f endMarker = playerTwo->GetTransform().GetPosition();

			  // Lerp
			  float x = lerp(startMarker.x, endMarker.x, m_cTimeStamp);
			  float y = lerp(startMarker.y, endMarker.y, m_cTimeStamp);

			  // Update Position
			  playerTwo->SetTransform(Transform(sf::Vector3f(x, y, 0), playerTwo->GetTransform().GetRotation(), playerTwo->GetTransform().GetScale()));
			  playerTwo->SetTransform(oldTrans);

			  //VerifyMove(playerTwo);
			  canMove = VerifyMove();
		  }



		  RakNet::BitStream bsOut;
		  bsOut.Write((RakNet::MessageID)ID_UPDATE_C);
		  ball.Serialize(true, &bsOut);
		  
		  playerOne->Serialize(true, &bsOut);
		  playerTwo->Serialize(true, &bsOut);
		  bsOut.Write(canMove);

		  mp_Peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mp_Packet->systemAddress, false);
		  
		  break;
		}
		case ID_UPDATE_C:
		{

		  RakNet::BitStream bsIn(mp_Packet->data, mp_Packet->length, false);
		  bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

		  sf::Vector3f startMarker = cBall.GetTransform().GetPosition();
		 // std::cout << "Old " << startMarker.x << ", " << startMarker.y << ", " << startMarker.z << std::endl;
		  // Need to lerp for ball here
		  cBall.Serialize(false, &bsIn);
		  Transform oldTrans = cBall.GetTransformBall();

		  sf::Vector3f endMarker = cBall.GetTransform().GetPosition();
		 // std::cout << "New " << endMarker.x << ", " << endMarker.y << ", " << endMarker.z << std::endl;

		  float x = lerp(startMarker.x, endMarker.x, m_cTimeStamp);
		  float y = lerp(startMarker.y, endMarker.y, m_cTimeStamp);

		  //std::cout << "x: " << x << "y: " << y << std::endl;

		  cBall.SetTransformBall(Transform(sf::Vector3f(x, y, 0), cBall.GetTransformBall().GetRotation(), cBall.GetTransformBall().GetScale()));

		  cBall.SetTransformBall(oldTrans);

		  if (m_PlayerNumber == 1)
		  {
			GameObject trash;
			trash.Serialize(false, &bsIn);
			cEnemy.Serialize(false, &bsIn);
		  }
		  else if (m_PlayerNumber == 2)
		  {
			cEnemy.Serialize(false, &bsIn);
			GameObject trash;
			trash.Serialize(false, &bsIn);
		  }

		  bsIn.Read(mCanMove);


		  // Send player pos to the server here.
		  RakNet::RakString rs;
		  RakNet::BitStream bsOut;
		  bsOut.Write((RakNet::MessageID)ID_UPDATE_S);
		  rs = std::to_string(m_PlayerNumber).c_str();
		  bsOut.Write(rs);
		  SendInputPacket(bsOut);
		  cPlayer.Serialize(true, &bsOut);
		  mp_Peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mp_Packet->systemAddress, false);
		  moveList.clear();
		  break;
		}
		case ID_PLAYER_NUM:
		{
			m_Connected = true;

			RakNet::RakString rs;
			RakNet::BitStream bsIn(mp_Packet->data, mp_Packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(rs);
			m_PlayerNumber = std::stoi(rs.C_String());

			if (m_PlayerNumber == 2)
			{
			  RakNet::BitStream bsOut;
			  bsOut.Write((RakNet::MessageID)ID_GAME_START_S);
			  mp_Peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mp_Packet->systemAddress, false);
			}

			break;
		}
		default:
		{
			std::cout << "Message with identifier " << mp_Packet->data[0] << " has arrived.\n";
			break;
		}
		}
	}
}

bool NetworkManager::VerifyMove()
{
	return true;
}