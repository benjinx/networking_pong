#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "SFML/System/Vector3.hpp"

class Transform
{
public:
  Transform();
  Transform(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float scaleX, float scaleY, float scaleZ);
  Transform(sf::Vector3f aPosition, sf::Vector3f aRotation, sf::Vector3f aScale);
  ~Transform() {};

  // Accessors
  sf::Vector3f GetPosition() { return _mPosition; }
  sf::Vector3f GetRotation() { return _mRotation; }
  sf::Vector3f GetScale() { return _mScale; }

  // Mutators
  void SetPosition(sf::Vector3f aPosition) { _mPosition = aPosition; }
  void SetPosition(float x, float y, float z) { _mPosition.x = x;
												_mPosition.y = y;
												_mPosition.z = z; 
											  }

  void SetRotation(sf::Vector3f aRotation) { _mRotation = aRotation; }
  void SetRotation(float x, float y, float z) {
												_mPosition.x = x;
												_mPosition.y = y;
												_mPosition.z = z;
											  }

  void SetScale(sf::Vector3f aScale) { _mScale = aScale; }
  void SetScale(float x, float y, float z) {
											  _mPosition.x = x;
											  _mPosition.y = y;
											  _mPosition.z = z;
										   }

private:
  sf::Vector3f _mPosition;
  sf::Vector3f _mRotation;
  sf::Vector3f _mScale;
};

#endif TRANSFORM_H