#ifndef NETWORK_MANAGER_H
#define NETWORK_MANAGER_H

#include <iostream>
#include <string>
#include <map>
#include <iterator>
#include <RakNet/RakPeerInterface.h>
#include <RakNet/RakString.h>
#include <RakNet/BitStream.h>
#include <RakNet/MessageIdentifiers.h>
#include "../../../src/Common/src/GameObject.h"
#include <cmath>
#include "SFML/System/Vector3.hpp"
#include "../../../src/Common/src/Move.h"

#define PLAYER_SIZE_X 10.0f
#define PLAYER_SIZE_Y 60.0f
#define BALL_SIZE 10.0f

#define PLAYER_ONE_START_POS_X 60.0f

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#define PLAYER_TWO_START_POS_X (SCREEN_WIDTH - PLAYER_ONE_START_POS_X)



enum PacketType
{
	PT_HELLO = 0,
	PT_INPUT_DATA,
	PT_REPLICATION_DATA,
	PT_DISCONNECT,
	PT_MAX
};

enum GameMessages
{
	ID_GAME_START_S = ID_USER_PACKET_ENUM + 1,
	ID_GAME_START_C = ID_USER_PACKET_ENUM + 2,
	ID_UPDATE_S = ID_USER_PACKET_ENUM + 3,
	ID_UPDATE_C = ID_USER_PACKET_ENUM + 4,
	ID_PLAYER_NUM = ID_USER_PACKET_ENUM + 5
};

class NetworkManager
{
public:
	NetworkManager();
	~NetworkManager() { RakNet::RakPeerInterface::DestroyInstance(mp_Peer); }

	void StartServer(int aServerPort, int aMaxClients);
	void StartClient(std::string anIPAddress, int aServerPort);

	void Update();

	void ProcessPacket(RakNet::BitStream& inInputStream, const RakNet::SystemAddress& inFromAddress);
	//void ProcessPacket(GameObject* inGameObject, RakNet::BitStream& inInputStream) {};
	void HandlePacketFromNewClient(RakNet::BitStream& inInputStream, const RakNet::SystemAddress& inFromAddress);
	void SendWelcomePacket(GameObject* newClientProxy);
	void SendInputPacket(RakNet::BitStream& outOutputStream);
	void RecieveInputPacket(RakNet::BitStream& inInputStream);
	void VerifyMove(GameObject* player);
	bool VerifyMove();
	void AddMove(Move& inMove);
	bool CanIMove() { return mCanMove; }
	std::vector<Move> GetMoveList() { return moveList; }

	void SetTimestamp(float timeStamp) { m_cTimeStamp = timeStamp; }

	float lerp(float v0, float v1, float t)
	{
		return (1 - t) * v0 + t * v1;
	}

	//Player stuff
	int m_PlayerNumber;
	bool m_Connected;
	bool m_AllConnected;
	//

	sf::Vector2f paddleDimensions = sf::Vector2f(PLAYER_SIZE_X, PLAYER_SIZE_Y);
	sf::Vector2f playerStartPosition = sf::Vector2f(60.0f, (SCREEN_HEIGHT / 2) - (PLAYER_SIZE_Y / 2));
	sf::Vector2f enemyStartPosition = sf::Vector2f(SCREEN_WIDTH - 60.0f, (SCREEN_HEIGHT / 2) - (PLAYER_SIZE_Y / 2));


	GameObject* playerOne;
	GameObject* playerTwo;
	GameObject ball = sf::CircleShape();
	GameObject cPlayer;
	GameObject cEnemy;
	GameObject cBall = sf::CircleShape();
	GameObject newObj;

	bool hasGameStarted = false;

private:
	RakNet::RakPeerInterface* mp_Peer;
	RakNet::Packet* mp_Packet;
	RakNet::SocketDescriptor m_SocketDescriptor;
	bool m_IsServer;
	int m_CurrentPlayerCount;
	std::map<std::string, RakNet::SystemAddress> m_PlayerAddresses;
	std::map<RakNet::SystemAddress, GameObject*> m_AddressToClientMap;
	int newConnection = 0;

	std::vector<Move> moveList;
	float m_cTimeStamp;
	sf::Time m_sTimeStamp;
	float moveListTimeStamps[3] = { 0, 0, 0 };

	bool mCanMove;

public:
  bool startBallMove = false;
};



inline uint32_t ConvertToFixed(float inNumber, float inMin, float inPrecision)
{
	return static_cast<uint32_t>((inNumber - inMin) / inPrecision);
}

inline float ConvertFromFixed(uint32_t inNumber, float inMin, float inPrecision)
{
	return static_cast<float>((inNumber)* inPrecision + inMin);
}

#endif NETWORK_MANAGER_H