#include "Transform.h"

Transform::Transform():
_mPosition(0.0f, 0.0f, 0.0f),
_mRotation(0.0f, 0.0f, 0.0f),
_mScale(1.0f, 1.0f, 1.0f)
{

}

Transform::Transform(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float scaleX, float scaleY, float scaleZ)
{
  _mPosition.x = posX;
  _mPosition.y = posY;
  _mPosition.z = posZ;

  _mRotation.x = rotX;
  _mRotation.y = rotY;
  _mRotation.z = rotZ;

  _mScale.x = scaleX;
  _mScale.y = scaleY;
  _mScale.z = scaleZ;
}

Transform::Transform(sf::Vector3f aPosition, sf::Vector3f aRotation, sf::Vector3f aScale)
{
  _mPosition = aPosition;
  _mRotation = aRotation;
  _mScale = aScale;
}
