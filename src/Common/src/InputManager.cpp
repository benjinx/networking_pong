#pragma once
#include "InputManager.h"
#include "Services.h"
#define PLAYER_SIZE_X 10.0f
#define PLAYER_SIZE_Y 60.0f
#define BALL_SIZE 10.0f

#define SCREEN_HEIGHT 600
#define SCREEN_WIDTH 800

#define MOVE_SPEED 0.1f

InputManager::InputManager()
:mIsInitialized(false)
{
}


InputManager::~InputManager()
{
	cleanup();
}

bool InputManager::init()
{
	if (mIsInitialized)
		return false;

	mIsInitialized = true;
	return true;
}

void InputManager::cleanup()
{
	if (mIsInitialized)
		mIsInitialized = false;
}

void InputManager::pollInput(bool isClient, float timeStamp, sf::RectangleShape& player)
{
	if (isClient)
	{
		Services::getNetworkManager()->SetTimestamp(timeStamp);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && g_canMove)
		{
			inputState.mDesiredForwardAmount = player.getPosition().y - MOVE_SPEED;
			m_move = Move(inputState, timeStamp, timeStamp / 3);
			Services::getNetworkManager()->AddMove(m_move);

			if (Services::getNetworkManager()->CanIMove()) {
				player.setPosition(sf::Vector2f(player.getPosition().x, player.getPosition().y - MOVE_SPEED));
				if (player.getPosition().y < 0)
					player.setPosition(sf::Vector2f(player.getPosition().x, 0));

				Services::getNetworkManager()->cPlayer.SetTransform(Transform(
					sf::Vector3f(player.getPosition().x, player.getPosition().y, 0.0f),
					sf::Vector3f(),
					sf::Vector3f(PLAYER_SIZE_X, PLAYER_SIZE_Y, 0.0f)));
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && g_canMove)
		{
			inputState.mDesiredBackAmount = player.getPosition().y + MOVE_SPEED;
			m_move = Move(inputState, timeStamp, timeStamp / 3);
			Services::getNetworkManager()->AddMove(m_move);

			if (Services::getNetworkManager()->CanIMove()) {
				player.setPosition(sf::Vector2f(player.getPosition().x, player.getPosition().y + MOVE_SPEED));
				if (player.getPosition().y > SCREEN_HEIGHT - PLAYER_SIZE_Y)
					player.setPosition(sf::Vector2f(player.getPosition().x, SCREEN_HEIGHT - PLAYER_SIZE_Y));

				Services::getNetworkManager()->cPlayer.SetTransform(Transform(
					sf::Vector3f(player.getPosition().x, player.getPosition().y, 0.0f),
					sf::Vector3f(),
					sf::Vector3f(PLAYER_SIZE_X, PLAYER_SIZE_Y, 0.0f)));
			}
		}
	}

	sf::Event inputEvent;

	while (Services::getGraphicsSystem()->getWindow()->pollEvent(inputEvent))
	{
		switch (inputEvent.type)
		{
		case sf::Event::Closed: // Window closed
			Services::getGraphicsSystem()->getWindow()->close();
			break;
		case sf::Event::KeyPressed:
			if (inputEvent.key.code == Input::Key::ESCAPE)
			{
				g_SPlayGame = false;
				g_CPlayGame = false;
				//Services::getEventSystem()->fireEvent(Event(EventType::GAME_EXIT));
			}
			else if (inputEvent.key.code == Input::Key::SPACE)
			{
				Services::getNetworkManager()->startBallMove = true;
				//Services::getEventSystem()->fireEvent(Event(EventType::MOVE_DOWN));
			}
			break;
		case sf::Event::LostFocus:
			g_canMove = false;
			break;
		case sf::Event::GainedFocus:
			g_canMove = true;
			break;
		default:
			break;
		}
	}
}