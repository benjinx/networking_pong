#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>

#include "Event.h"
#include "InputState.h"
#include "Move.h"

namespace Input{
	enum Key
	{
		TILDE = sf::Keyboard::Tilde,
		LEFT_ARROW = sf::Keyboard::Left,
		RIGHT_ARROW = sf::Keyboard::Right,
		DOWN_ARROW = sf::Keyboard::Down,
		UP_ARROW = sf::Keyboard::Up,
		W = sf::Keyboard::W,
		A = sf::Keyboard::A,
		S = sf::Keyboard::S,
		D = sf::Keyboard::D,
		ESCAPE = sf::Keyboard::Escape,
		SPACE = sf::Keyboard::Space,
		ENTER = sf::Keyboard::Return,
	};

	enum Mouse
	{
		LEFT = sf::Mouse::Left,
		RIGHT = sf::Mouse::Right,
		MIDDLE = sf::Mouse::Middle
	};
}

class InputManager
{
public:
	InputManager();
	~InputManager();

	// Functions
	bool isMouseDown(Input::Mouse button);
	bool isMousePressed(Input::Mouse button);
	bool isMouseReleased(Input::Mouse button);
	bool init();
	void cleanup();
	void pollInput(bool isClient = false, float timeStamp = 0.0f, sf::RectangleShape& player = sf::RectangleShape());

	bool g_SPlayGame = true;
	bool g_CPlayGame = true;
	bool g_canMove = true;

	InputState inputState;
	Move m_move;

private:
	bool mIsInitialized;
	sf::Event::MouseButtonEvent* mpSDLMouseState;
	sf::Event::MouseButtonEvent* mpSDLMouseStatePrev;

};

#endif