#ifndef MOVE_H
#define MOVE_H

#include "InputState.h"

class Move {
public: Move() {}
		Move(const InputState& inInputState, float inTimestamp,
			float inDeltaTime) :
			mInputState(inInputState),
			mTimestamp(inTimestamp),
			mDeltaTime(inDeltaTime)
			{}

		const Move& AddMove(const InputState& inInputState, float inTimeStamp);

		const InputState& GetInputState() const { return mInputState; }
		float GetTimestamp() const { return mTimestamp; }
		float GetDeltaTime() const { return mDeltaTime; }
		void Write(RakNet::BitStream& inOutputStream) const
		{
			inOutputStream.Serialize(true, mInputState);
			inOutputStream.Serialize(true, mTimestamp);
			inOutputStream.Serialize(true, mDeltaTime);
		}
		void Read(RakNet::BitStream& inInputStream)
		{
			inInputStream.Serialize(false, mInputState);
			inInputStream.Serialize(false, mTimestamp);
			inInputStream.Serialize(false, mDeltaTime);
		}

private:
	InputState mInputState;
	float mTimestamp;
	float mDeltaTime;
};

#endif MOVE_H