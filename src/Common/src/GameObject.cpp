#include "GameObject.h"
#include <iostream>
#include "Services.h"

GameObject::GameObject()
{
  _mRec.setSize(sf::Vector2f(_mTransform.GetScale().x, _mTransform.GetScale().y));
  _mRec.setPosition(_mTransform.GetPosition().x, _mTransform.GetPosition().y);
  _mRec.setFillColor(sf::Color::White);
}

GameObject::GameObject(Transform aTransform)
{
  _mTransform = aTransform;
  _mRec.setSize(sf::Vector2f(_mTransform.GetScale().x, _mTransform.GetScale().y));
  _mRec.setPosition(_mTransform.GetPosition().x, _mTransform.GetPosition().y);
  _mRec.setFillColor(sf::Color::White);
}

GameObject::GameObject(sf::RectangleShape aShape)
{
  _mRec = aShape;
  _mRec.setSize(sf::Vector2f(_mTransform.GetScale().x, _mTransform.GetScale().y));
}

GameObject::GameObject(Transform aTransform, sf::RectangleShape aShape)
{
  _mTransform = aTransform;
  _mRec = aShape;
  _mRec.setSize(sf::Vector2f(_mTransform.GetScale().x, _mTransform.GetScale().y));
}

GameObject::GameObject(sf::CircleShape aShape)
{
  _mBall = aShape;
  _mBall.setRadius(_mTransform.GetScale().x);
}

GameObject::~GameObject()
{

}

bool GameObject::Intersects(GameObject aGameObject)
{
  // Do collision check and return if it intersects.
  if (_mRec.getGlobalBounds().intersects(aGameObject._mRec.getGlobalBounds()))
  {
	std::cout << "Intersects\n" << std::endl;
	return true;
  }
  else
	return false;
}

void GameObject::Serialize(bool writeToBitstream, RakNet::BitStream* bs)
{
  if (writeToBitstream)
  {
	sf::Vector3<uint32_t> sendPos(ConvertToFixed(_mTransform.GetPosition().x, 0.0f, 0.1f),
								  ConvertToFixed(_mTransform.GetPosition().y, 0.0f, 0.1f),
								  ConvertToFixed(_mTransform.GetPosition().z, 0.0f, 0.1f));
	sf::Vector3<uint32_t> sendSize(ConvertToFixed(_mTransform.GetScale().x, 0.0f, 0.1f),
								   ConvertToFixed(_mTransform.GetScale().y, 0.0f, 0.1f),
								   ConvertToFixed(_mTransform.GetScale().z, 0.0f, 0.1f));
	
	bs->Serialize(writeToBitstream, sendPos);
	bs->Serialize(writeToBitstream, sendSize);
  }
  else if (!writeToBitstream)
  {
	sf::Vector3<uint32_t> tempPos, tempSize;
	bs->Serialize(writeToBitstream, tempPos);
	bs->Serialize(writeToBitstream, tempSize);

	sf::Vector3f newPos(ConvertFromFixed(tempPos.x, 0.0f, 0.1f),
						ConvertFromFixed(tempPos.y, 0.0f, 0.1f),
						ConvertFromFixed(tempPos.z, 0.0f, 0.1f));

	sf::Vector3f newSize(ConvertFromFixed(tempSize.x, 0.0f, 0.1f),
						 ConvertFromFixed(tempSize.y, 0.0f, 0.1f),
						 ConvertFromFixed(tempSize.z, 0.0f, 0.1f));

	_mTransform.SetPosition(newPos);
	_mTransform.SetScale(newSize);

	_mRec.setSize(sf::Vector2f(_mTransform.GetScale().x, _mTransform.GetScale().y));
	_mRec.setPosition(_mTransform.GetPosition().x, _mTransform.GetPosition().y);

	_mBall.setRadius(_mTransform.GetScale().x);
	_mBall.setPosition(_mTransform.GetPosition().x, _mTransform.GetPosition().y);
  }

}